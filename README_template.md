# Software Studio 2020 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Name of widgets                                  | 1~5%     | Y         |
| change background                                |          | Y         |
| canvas Transparent                               |          | Y         |
| music player                                     |          | Y         |

---

### How to use
    我把功能欄大致分成4個部分
#### 一、工具欄
    第一排是畫筆、橡皮擦和畫直線
    第二排是畫方形、圓形和三角形
    第三排是添加文字和上傳圖片
    
    當鼠標移到buttom上時(除了上傳圖片)，會改變鼠標的圖案，click之後在畫布上的鼠標圖案也會跟著改變。
![](https://i.imgur.com/n3qRpbO.png)圖1
#### 二、調整筆寬和顏色 + 調整文字的大小和字形
    輸入文字的部分，先在輸入框(圖3)裡面輸入內容，然後在工具欄裡面點擊文字的buttom後，在畫布上click後就會看到字出現在畫布的鼠標位置。

點方框可以選擇畫筆的顏色，滑動bar可以調整筆寬。
![](https://i.imgur.com/guYATNT.png)圖2

虛線內可以輸入想放入canvas的內容，size是選字的大小，font是選字型。
![](https://i.imgur.com/HYdRp4U.png)圖3
#### 三、其他基本功能
    undo = 上一步
    redo = 下一步
    save = 下載(jpg)
    clear = 清空
![](https://i.imgur.com/NDy5WS2.png) 圖4

### 四、其他bonus功能
    圖5:
        click圈圈們可以把背景改成跟圈圈內顏色一樣。
        勾選框框可以把畫布變成透明的，雖然下載的結果不會有什麼變化，但是可以看到好看的流星雨:D。
    圖6:
        是個滿基本的音樂撥放器，歌名是毛不易的不染(好聽)。

![](https://i.imgur.com/nDBoIHz.png) 圖5

![](https://i.imgur.com/SyL5W6K.png) 圖6

### Function description
    我做了4個bonus，前3個有功能，最後一個是外觀的部分
1.當onclick觸發時，會在js裡面改body的background-Image
![](https://i.imgur.com/n8Co5zF.png)

2.當框框被打勾時，會在js裡面，把canvas的background-color移除。
  框框沒被打勾時，會在js裡面，把canvas的background-color設成white。
![](https://i.imgur.com/CbmDAO0.png)

3.音樂撥放器設定成自動撥放+循環撥放
![](https://i.imgur.com/QLoexTE.png)

4.我把光點的動畫加入背景裡面，設定右上起始跟左下中止，然後隨機產生速率跟出現位置，就能呈現流星雨的感覺了。

    
    

### Gitlab page link
https://107062222.gitlab.io/AS_01_WebCanvas

### Others (Optional)

    感謝辛苦的助教們

<style>
table th{
    width: 100%;
}
</style>