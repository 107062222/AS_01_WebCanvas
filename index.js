var canvas = document.getElementById("canvas");

var ctx = canvas.getContext("2d");
ctx.strokeStyle = 'black'; //預設白色
ctx.lineWidth = '3px';
ctx.font = '13px Comic Sans MS';
ctx.lineJoin = 'round';
ctx.lineCap = 'round'; 

/*----------------------選畫筆粗細--------------------------------*/
function ChangeWidth() {
    ctx.lineWidth = document.getElementById("width").value;
}
/*-------------------------選畫筆色------------------------------*/
function ChangeColor() {
    var x = document.getElementById("Color").value;
    ctx.strokeStyle = x;
}

var fontsize, font;
/*-------------------選字大小----------------------------*/
function ChangeFontSize() {
    fontsize = document.getElementById("fontsize").value;
}
/*-------------------選字體----------------------------*/
function ChangeFont(input) {
    font = input;
}
/*--------------選背景-------------------*/
function ChangeCanvas(number){
    if(number == 0)
        document.getElementById("body").style.backgroundImage = "url('source/background.jpg')";
    else if(number == 1)
        document.getElementById("body").style.backgroundImage = "none";
    else if(number == 2)
        document.getElementById("body").style.backgroundImage = "linear-gradient(to top, #09203f 0%, #537895 100%)";
    else if(number == 3)
        document.getElementById("body").style.backgroundImage = "linear-gradient(to right, #ff8177 0%, #ff867a 0%, #ff8c7f 21%, #f99185 52%, #cf556c 78%, #b12a5b 100%)";
    else if(number == 4)
        document.getElementById("body").style.backgroundImage = "linear-gradient(to top, #30cfd0 0%, #330867 100%)";
    else if(number == 5)
        document.getElementById("body").style.backgroundImage = "linear-gradient(to right, #eea2a2 0%, #bbc1bf 19%, #57c6e1 42%, #b49fda 79%, #7ac5d8 100%)";
}
/*---------選畫布-------------------------*/
function Transparent(){
    var C = document.getElementById("Transparent");
    if(C.checked)
        $("canvas").css("background-color", "");    
    else
        document.getElementById("canvas").style.backgroundColor = "white";
        

}

/*---------選工具----------*/
let imgpen;
let tool = 0;
function pencil() {
    tool = 0;
    document.getElementById("canvas").style.cursor= "url('source/pe.png') 0 32, auto";
};
function erase() {
    tool = 1;
    document.getElementById("canvas").style.cursor = "url('source/EE.png') 0 32, auto";
}
function line() {
    tool = 2;
    document.getElementById("canvas").style.cursor = "url('source/LL.png'), auto";
}
function rect() {
    tool = 3;
    document.getElementById("canvas").style.cursor = "url('source/RR.png'), auto";
}
function circle() {
    tool = 4;
    document.getElementById("canvas").style.cursor = "url('source/CC.png'), auto"
}
function txt() {
    tool = 5;
    document.getElementById("canvas").style.cursor = "url('source/TT.png'), auto"
}
function triangle() {
    tool = 6;
    document.getElementById("canvas").style.cursor= "url('source/TRTR.png') 0 32, auto";
};
/*----------end-------------/

/*------------------監控滑鼠-------------------*/
let down = false;
let lx = 0;
let ly = 0;
let redo = [];
let undo = [];
let canvas2;

$('#canvas').mousedown(function(e){
    down = true;
    lx = e.offsetX;
    ly = e.offsetY;
    
    Add();
    canvas2 =  new Image();
    canvas2.src = canvas.toDataURL();

    if(tool == 5)
        drawTxet(e);
}).mouseup(function(){
    ctx.globalCompositeOperation = "source-over";
    down = false;
}).mousemove(function(e){
    if(tool == 0)
        drawPencil(e);
    else if(tool == 1)
        drawErase(e);
    else if(tool == 2)
        drawLine(e);
    else if(tool == 3)
        drawRect(e);
    else if(tool == 4)
        drawCircle(e);
    /*else if(tool == 5)
        drawTxet(e);*/
    else if(tool == 6)
        drawTriangle(e);
    /*else 
        drawPencil(e);*/
}).mouseout(function(){
    ctx.globalCompositeOperation = "source-over";
    down = false;
})

window.addEventListener('load', function(){
    document.getElementById("canvas").style.backgroundColor = "white";
});

/*---------------------end------------------------------*/

/*-----------------------tool功能-------------------------*/
function drawPencil(e){
    if(!down) return;

    ctx.beginPath();
    ctx.moveTo(lx, ly);
    ctx.lineTo(e.offsetX, e.offsetY);
    
    ctx.stroke();
    ctx.closePath();
    //ctx.save();
    [lx, ly] = [e.offsetX, e.offsetY];
}
function drawErase(e){
    if(!down) return;

    ctx.globalCompositeOperation = "destination-out";
    ctx.beginPath();
    ctx.moveTo(lx, ly);
    ctx.lineTo(e.offsetX, e.offsetY);
    
    ctx.stroke();
    ctx.closePath();
    //ctx.save();
    [lx, ly] = [e.offsetX, e.offsetY];
}
function drawLine(e){
    if(!down) return;

    //ctx.clearRect(0, 0, canvas.width, canvas.height);
    maintain()
    ctx.beginPath();
    ctx.moveTo(lx, ly);
    ctx.lineTo(e.offsetX, e.offsetY);

    ctx.stroke();
    ctx.closePath();
}
function drawRect(e){
    if(!down) return;

    maintain();
    ctx.beginPath();
    
    ctx.strokeRect(lx, ly, e.offsetX - lx, e.offsetY - ly);
    ctx.closePath();
}
function drawCircle(e){
    if(!down) return;
    
    maintain();
    ctx.beginPath();
    
    var rx = (e.offsetX - lx)/2;
    var ry = (e.offsetY - ly)/2;
    var r = Math.sqrt(rx*rx+ry*ry);
    ctx.arc(rx + lx, ry + ly,r,0,Math.PI*2); // 第5个参数默认是false-顺时针
    
    ctx.stroke();
    ctx.closePath();
}
function drawTriangle(e){
    if(!down) return;
    maintain();
    ctx.beginPath();
    ctx.moveTo(lx, ly);
    ctx.lineTo(e.offsetX, e.offsetY);
    ctx.lineTo(e.offsetX-2*(e.offsetX - lx), e.offsetY);
    ctx.lineTo(lx, ly);
    ctx.stroke();
}
function drawTxet(e){
    /*if(!down) return

    maintain();*/
    ctx.beginPath();
    ctx.font = fontsize+"px "+ font;
    var txt = document.getElementById("Word").value;

    ctx.fillText(txt, e.offsetX, e.offsetY);
    ctx.closePath();
}

const file = document.querySelector('#file');
file.addEventListener('change', function(e){
    const photo = e.target.files[0];
    let img = new Image();
    img.src = URL.createObjectURL(photo);

    img.onload = function(){
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        ctx.drawImage(img, 0, 0, img.width, img.height);
        Add();
    }
})

/*-----------------------end-----------------------*/

/*-----------------上一步、下一步---------------------*/
function maintain(){

    ctx.clearRect(0, 0, canvas.width, canvas.height); // 清空旧 canvas
    ctx.drawImage(canvas2, 0, 0); // 将缓存 canvas 复制到旧的 canvas  
}

function Add() {
    undo.push(canvas.toDataURL());
    redo = [];
}
function Undo() {
    if(undo.length > 0){

        let canvas2 = new Image();
        canvas2.src = undo.pop();
        canvas2.onload = function() {
            
            redo.push(canvas.toDataURL());
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            ctx.drawImage(canvas2, 0, 0);
        }
    }
}
function Redo() {
    if (redo.length > 0) {
    
        const canvas2 = new Image(); //建立新的 Image
        canvas2.src = redo.pop(); //載入剛剛存放的影像
        canvas2.onload = function() {
            
            undo.push(canvas.toDataURL());
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            ctx.drawImage(canvas2, 0, 0) //匯出影像並從座標 x:0 y:0 開始
        }
    }
}

function Clear(){
    Add();
    ctx.clearRect(0, 0, canvas.width, canvas.height);
}
/*----------------------end---------------------------------*/

/*------------------------流星雨--------------------------*/
var stars = document.getElementById('stars')

// js随机生成流星
for (var j = 0; j < 60; j++) {
    var newStar = document.createElement("div")
    newStar.className = "star"
    newStar.style.top = randomDistance(500, -100) + 'px'
    newStar.style.left = randomDistance(1980, 300) + 'px'
    stars.appendChild(newStar)
}

// 封装随机数方法
function randomDistance(max, min) {
    var distance = Math.floor(Math.random() * (max - min + 1) + min)
    return distance
}

var star = document.getElementsByClassName('star')

// 给流星添加动画延时
for (var i = 0, len = star.length; i < len; i++) {　　
    star[i].style.animationDelay = i % 6 == 0 ? '0s' : i * 0.8 + 's'
}

/*-------------------------------------------------------*/
